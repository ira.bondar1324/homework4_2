"use strict";

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];
const needProp=[`author`,`name`,`price`];
const list =document.createElement("ul");
const div=document.querySelector("#root");
for (const elem of books) {
  try{
    let count=0;
    for (const el of needProp) {
      if(elem.hasOwnProperty(el)){
        count++;
      }else throw new Error(`Немає властивості ${el}`);
    }
    if(count===needProp.length){
      const listElem =document.createElement("li");
      listElem.textContent=`${Object.values(elem)} `;
      list.append(listElem);
    }
  }
  catch(err){
    console.log(err.message);
  }
}
div.append(list);
